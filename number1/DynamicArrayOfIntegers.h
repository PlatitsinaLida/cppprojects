#ifndef NUMBER1_DYNAMICARRAYOFINTEGERS_H
#define NUMBER1_DYNAMICARRAYOFINTEGERS_H

#include <ostream>

using namespace std;
class DynamicArrayOfIntegers {
private:
    int *array;
    int size;

public:
    DynamicArrayOfIntegers();//Конструктор по умолчанию
    DynamicArrayOfIntegers(int size);//Конструктор по размеру
    DynamicArrayOfIntegers(int size, int n);//Конструктор  по размеру и числу n
    DynamicArrayOfIntegers(const DynamicArrayOfIntegers& copy);//Конструктор копирования
    DynamicArrayOfIntegers(DynamicArrayOfIntegers&& temp);//Конструктор перемещения
    ~DynamicArrayOfIntegers();//Деструктор

    int getLength();//длина массива
    int operator [](int x);//доступ к элементу
    bool resize(int newSize);//изменение размера
    DynamicArrayOfIntegers operator =(const DynamicArrayOfIntegers&  m1);//Оператор присваивания
    DynamicArrayOfIntegers operator =(DynamicArrayOfIntegers&&  m1);//оператор перемещения

    bool operator==(const DynamicArrayOfIntegers &m1) const;//перегрузка==
    bool operator!=(const DynamicArrayOfIntegers &m1) const;//перегрузка !=

    bool operator >(DynamicArrayOfIntegers const &m1);//перегрузка >
    bool operator <(DynamicArrayOfIntegers const &m1);//перегрузка <
    bool operator >=(DynamicArrayOfIntegers const &m1);//перегрузка >=
    bool operator <=(DynamicArrayOfIntegers const &m1);// перегрузка <=

    DynamicArrayOfIntegers& operator + (const DynamicArrayOfIntegers& m2);//сложение массивов

    friend std::ostream &operator<<(std::ostream &, const DynamicArrayOfIntegers  &);//оператор вывода
    friend std::istream &operator>>(std::istream &in, DynamicArrayOfIntegers &);//оператор ввода

    void toString();
    int getElem(int i);
};


#endif //NUMBER1_DYNAMICARRAYOFINTEGERS_H
