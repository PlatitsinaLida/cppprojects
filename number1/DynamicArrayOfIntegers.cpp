#include <iostream>
#include "DynamicArrayOfIntegers.h"

using namespace std;

struct SizeException{};


//Конструктор по умолчанию
DynamicArrayOfIntegers::DynamicArrayOfIntegers(){
    size = 1;
    array = new int[size];
}

//Конструктор по размеру
DynamicArrayOfIntegers::DynamicArrayOfIntegers(int size){
    this->size = size;
    array = new int[size];
    for (int i = 0; i < size; i++) {
        array[i] = 0;
    }
}

//Конструктор  по размеру и числу n
DynamicArrayOfIntegers::DynamicArrayOfIntegers(int size, int n){
    this->size = size;
    array = new int[size];
    for (int i = 0; i < size; i++) {
        array[i] = n;
    }
}

//Конструктор копирования
DynamicArrayOfIntegers::DynamicArrayOfIntegers(const DynamicArrayOfIntegers& copy){
    size = copy.size;
    array = new int[size];
    for (int i = 0; i < size; i++) {
        array[i] = copy.array[i];
    }
}

//Конструктор перемещения
DynamicArrayOfIntegers::DynamicArrayOfIntegers(DynamicArrayOfIntegers&& temp){
    size = temp.size;
    array = new int[size];
    for (int i = 0; i < size; i++) {
        array[i] = temp.array[i];
    }
    temp.size = 1;
    temp.array = new int[1];
}

//Деструктор
DynamicArrayOfIntegers::~DynamicArrayOfIntegers(){
    delete []array;
    array = NULL;
    size = -1;
}

void DynamicArrayOfIntegers::toString() {
    for (int i = 0; i < size; i++) {
        cout << array[i] << " ";
    }
    cout << "\n";
}

//длина массива
int DynamicArrayOfIntegers::getLength(){
    return size;
}

int DynamicArrayOfIntegers::getElem(int i) {
    return array[i];
}

//доступ к элементу
int DynamicArrayOfIntegers::operator [](int x){
    if (x > this->size && x<0) {
        throw new SizeException();
    }
    return array[x];
}

//изменение размера
bool DynamicArrayOfIntegers::resize(int newSize){
    if(newSize<=0){ throw SizeException();}
   int* tempArray = new int[newSize];
    if (size > newSize) {
        for (int i = 0; i < newSize; i++) {
            tempArray[i] = array[i];
        }
    }
    else {
        for (int j = 0; j < size; j++) {
            tempArray[j] = array[j];
        }
        for (int i = size; i < newSize; i++) {
            tempArray[i] = 0;
        }
    }
    size = newSize;
    delete []array;
    array = NULL;
    array = tempArray;
    tempArray = NULL;
    delete tempArray;
}

//оператор присваивания
DynamicArrayOfIntegers DynamicArrayOfIntegers:: operator =(const DynamicArrayOfIntegers&  m1){
    this->size = m1.size;
    this->array = new int[m1.size];
    for (int i = 0; i < m1.size; i++) {
        this->array[i] = m1.array[i];
    }
    return *this;
}

//оператор перемещения
DynamicArrayOfIntegers DynamicArrayOfIntegers:: operator =(DynamicArrayOfIntegers&&  m1){
    this->size = m1.size;
    this->array = new int[m1.size];
    for (int i = 0; i < m1.size; i++) {
        this->array[i] = m1.array[i];
    }
    m1.size = 1;
    m1.array = new int[m1.size];
}

//перегрузка==
bool DynamicArrayOfIntegers::operator==(const DynamicArrayOfIntegers &m1) const{
    if (m1.size != this->size) {
        throw new SizeException();
    }
    for (int i = 0; i < m1.size; i++) {
        if (m1.array[i] != this->array[i]) {
            return false;
        }
    }
    return true;
}

//перегрузка !=
bool DynamicArrayOfIntegers::operator!=(const DynamicArrayOfIntegers &m1) const{
    if (m1.size != this->size) {
        throw new SizeException();
    }
    for (int i = 0; i < m1.size; i++) {
        if (m1.array[i] != this->array[i]) {
            return true;
        }
    }
    return false;
}

//перегрузка >
bool DynamicArrayOfIntegers::operator >(DynamicArrayOfIntegers const &m1){
    if (m1.size < this->size) {
        for (int i = 0; i < m1.size; i++) {
            if (m1.array[i] < this->array[i])
                return true;
        }return true;
    }
    if (m1.size >= this->size) {
        for (int j = 0; j < this->size; j++) {
            if (m1.array[j] < this->array[j]) {
                return true;
            }
        }
    }
    return false;
}

//перегрузка <
bool DynamicArrayOfIntegers::operator <(DynamicArrayOfIntegers const &m1){
    if (m1.size <= this->size) {
        for (int i = 0; i < m1.size; i++) {
            if (m1.array[i] < this->array[i])
                return true;
        }
    }
    if (m1.size > this->size) {
        for (int j = 0; j < this->size; j++) {
            if (m1.array[j] < this->array[j]) {
                return true;
            }
        }return true;
    }
    return false;
}

//перегрузка >=
bool DynamicArrayOfIntegers::operator >=(DynamicArrayOfIntegers const &m1){
    if (m1.size < this->size) { return true; }
    if (m1.size > this->size) { return false;}
    if (m1.size == this->size) {
        for (int i = 0; i < m1.size; i++) {
            return (m1.array[i] < this->array[i]) || (m1.array[i] == this->array[i]);

        }
    }
}

// перегрузка <=
bool DynamicArrayOfIntegers::operator <=(DynamicArrayOfIntegers const &m1){
    if (m1.size < this->size) { return false; }
    if (m1.size > this->size) { return true; }
    if (m1.size == this->size) {
        for (int i = 0; i < m1.size; i++) {
            if ((m1.array[i] > this->array[i]) || (m1.array[i] == this->array[i])) {
                return true;
            }
            return false;

        }
    }
}

//сложение массивов
DynamicArrayOfIntegers& DynamicArrayOfIntegers::operator + (const DynamicArrayOfIntegers& m2){
    int bigSize = this->size + m2.size;
    DynamicArrayOfIntegers *res = new DynamicArrayOfIntegers(bigSize);
    for (int i = 0; i < this->size; i++) {
        res->array[i] = this->array[i];
    }
    for (int j = size; j < bigSize; j++) {
    res->array[j] = m2.array[j - size];
    }
    res->size = bigSize;
    return *res;
}

//оператор вывода
std::ostream& operator<<(ostream & os, const DynamicArrayOfIntegers& dinArray){
    os << "size :" << dinArray.size << endl;
    os << "array {";
    for (int i = 0; i < dinArray.size; i++) {
        os << dinArray.array[i] << " ";
    }
    os << "}";
    return os;
}

//оператор ввода
std::istream& operator>>(istream &is, DynamicArrayOfIntegers &dinArray){
    is >> dinArray.size;
    delete[] dinArray.array;
    dinArray.array = new int[dinArray.size];
    for (int i = 0; i < dinArray.size; i++) {
        is >> dinArray.array[i];
    }
    return is;
}

