#ifndef NUMBER4_LINKEDHASHTABLE_H
#define NUMBER4_LINKEDHASHTABLE_H
struct NoElemException{};


struct Element
{
    int data;
    int key;
    Element* next;
    Element(int data, int key, Element* next = nullptr);
};

class HashList {
private:
    Element* first;
    Element* last;
public:
    friend class Iterator;
    HashList();
    ~HashList();
    void addElem(int elem, int key);//добавить элемент
    void delElem(int elem);//удалить элемент
    void doEmpty();//сделать таблицу пустой
};

class LinkedHashTable
{
private:
    Element** hashTable;
    HashList* list;
    int size;
    int countElem;
public:
    friend class Iterator;
    LinkedHashTable();
    LinkedHashTable(int size);
    void addElem(int elem, int key);//добавить элемент
    void removeElem(int key);
    int findElem(int key);//найти элемент
    void doEmpty();//сделать таблицу пустой
    bool isEmpty();//проверка таблицы на пустоту
    void printElem();
    ~LinkedHashTable();
};


class Iterator {
private:
    Element* now;
    LinkedHashTable* hashList;

public:
    Iterator(LinkedHashTable* &hashTable);
    ~Iterator();
    bool hasNext();
    void start();
    void next();
    int seeElem();
};

#endif //NUMBER4_LINKEDHASHTABLE_H
