#ifndef NUMBER3_LIST_H
#define NUMBER3_LIST_H

#include "Iterator.h"

struct BufferException {};
struct NoElemException {};

class List {
public:
    //вставить элемент в позицию, отмеченную итератором
	virtual void addElem(const TElem &elem, Iterator &iter) = 0;
    //удалить элемент, отмеченный итератором
	virtual void deleteElem(Iterator &iter) = 0;
	//найти первое вхождение элемента в список, результат – итератор на найденный элемент
	virtual Iterator* findElem(const TElem &elem) = 0;
	//сделать список пустым
	virtual void makeEmpty() = 0;
	//проверка на пустоту
	virtual bool isEmpty() const = 0;
	//количество элементов в списке
	virtual int getSize() const = 0;
	//получить итератор на первый элемент списка
	virtual Iterator* getIterator() = 0;
};


#endif //NUMBER3_LIST_H
