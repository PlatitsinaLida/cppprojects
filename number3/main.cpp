#include "BufferList.h"

#include <iostream>

int main() {
    BufferList *firstBL= new BufferList();
    BufferList::ListIterator itr(firstBL);
    firstBL->addElem(1,itr);
    itr.next();
    firstBL->addElem(3, itr);
    firstBL->addElem(2, itr);
    itr.start();
    while (!itr.finish())
    {
        itr.next();
        std::cout << itr.getElement() << std::endl;
    }
    std::cout << "--------------------------------" << std::endl;
    Iterator *itr2 = firstBL->getIterator();
    itr2->next();
    std::cout << itr2->getElement() << std::endl;
    Iterator *itr3 = firstBL->findElem(3);
    firstBL->deleteElem(*itr3);
    itr.start();
    std::cout << "--------------------------------" << std::endl;
    while (!itr.finish())
    {
        itr.next();
        std::cout << itr.getElement() << std::endl;
    }
    std::cout << "--------------------------------" << std::endl;
    std::cout <<"Size: "<< firstBL->getSize() << std::endl;
    std::cout << "isEmpty: (1-yes, 0-no) " << firstBL->isEmpty() << std::endl;
    firstBL->makeEmpty();
    std::cout << "isEmpty: (1-yes, 0-no)" << firstBL->isEmpty() << std::endl;
    itr.start();
    std::cout << "--------------------------------" << std::endl;
    while (!itr.finish())
    {
        itr.next();
        std::cout << itr.getElement() << std::endl;
    }
    return 0;
}
