#ifndef NUMBER3_BUFFERLIST_H
#define NUMBER3_BUFFERLIST_H

#include "List.h"

//Класс-реализация списка (закольцованный список с буферным элементом).
//Должны быть реализованы методы базового класса, конструкторы по умолчанию,
//копирования и перемещения, операторы присваивания.
class BufferList : public List
{
private:
    Node* head;
    int size;

public:
    friend class ListIterator;
    class ListIterator : public Iterator {
    private:
        BufferList *list;
        Node * nowPointer;//текущий узел
    public:
        ListIterator(BufferList *list);
        void start();
        TElem getElement() const;
        void next();
        void prev();
        bool finish() const;
        Node* getNow() const;
    };

    void addElem(const TElem &elem, Iterator &iter);
    void deleteElem(Iterator &iter);
    void makeEmpty();
    bool isEmpty() const;
    int getSize() const;
    Iterator* findElem(const TElem &elem);
    Iterator* getIterator();
    BufferList();
    BufferList(const BufferList& copy);
    BufferList(BufferList&& copy);
    BufferList& operator= (const BufferList &copy);
    BufferList& operator=(BufferList&& copy);
    ~BufferList();
};

#endif //NUMBER3_BUFFERLIST_H
