#ifndef NUMBER3_ITERATOR_H
#define NUMBER3_ITERATOR_H

#include "iostream"
struct EndOfIterator {};
typedef int TElem;
struct Node
{
    TElem data;
    Node *next;
    Node *prev;

    Node(TElem data, Node* next = nullptr, Node* prev = nullptr) {
        this->data = data;
        this->next = next;
        this->prev = prev;
    }
};

class Iterator {
public:
    virtual void start() = 0;//начать работу
    virtual TElem getElement() const = 0;//получить очередной элемент
    virtual void next() = 0;//сдвинуть итератор на следующий элемент
    virtual void prev() = 0;
    virtual bool finish() const = 0;//проверка, все ли проитерировано
    virtual Node* getNow() const = 0;
};


#endif //NUMBER3_ITERATOR_H
