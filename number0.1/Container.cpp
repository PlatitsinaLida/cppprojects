
#include "Container.h"

using namespace std;
namespace lp {
    Container::Container(int leng, int wid, int heig, double weig, int val) {
        length = leng;
        width = wid;
        height = heig;
        weight = weig;
        value = val;
    }

    vector<Box> &Container::getVector() {
        return vector;
    }

    void Container::setVector(std::vector<Box> &vector) {
        Container::vector = vector;
    }

    int Container::getLength() {
        return length;
    }
    void Container::setLength(int length) {
        Container::length = length;
    }

    int Container::getWidth() {
        return width;
    }
    void Container::setWidth(int width) {
        Container::width = width;
    }

    int Container::getHeight() {
        return height;
    }
    void Container::setHeight(int height) {
        Container::height = height;
    }

    double Container::getWeight() {
        return weight;
    }
    void Container::setWeight(double weight) {
        Container::weight = weight;
    }

    int Container::getValue() {
        return value;
    }
    void Container::setValue(int value) {
        Container::value = value;
    }

// количество коробок в контейнере
    int Container::count() {
        return getVector().size();
    }

//суммарный вес содержимого контейнера
    double Container::countWeight() {
        double countWeight = 0;
        for (int i = 0; i < count(); i++) {
            countWeight += vector[i].getWeight();
        }
        return countWeight;
    }

// суммарная стоимость содержимого
    int Container::countValue() {
        int countValue = 0;
        for (int i = 0; i < count(); i++) {
            countValue += vector[i].getValue();
        }
        return countValue;
    }

// получение коробки по индексу
    Box Container::getBoxByIndex(int index) {
        return vector.at(index);
    }

// добавление коробки в контейнер
    int Container::addContainer(Box box) {
        double sumWeight = 0;
        static int num = 0;
        if (box.getLength() > length && box.getWidth() > width &&
            box.getHeight() > height && box.getWeight() > weight &&
            box.getValue() > value) { throw -2; }
        else {
            for (int i = 0; i < vector.size(); i++) {
                sumWeight += getBoxByIndex(i).getWeight();
            }
            if (weight > sumWeight) {
                vector.push_back(box);
                num++;
                return num;
            } else {
                throw -1;
            }
        }
    }

// удаление коробки по индексу
    void Container::clearElem(int index) {
        if (vector.size() == 0) { throw -3; }
        if (index > vector.size()) { throw -4; }
        else {
            vector.erase(vector.begin() + index);
        }
    }

//оператор ввода
    std::istream &operator>>(std::istream &in, Container &cont) {
        cout << "Input length:" << endl;
        in >> cont.length;
        cout << "Input width:" << endl;
        in >> cont.width;
        cout << "Input height:" << endl;
        in >> cont.height;
        cout << "Input weight:" << endl;
        in >> cont.weight;
        cout << "Input value:" << endl;
        in >> cont.value;

        return in;
    }

//оператор ввывода
    std::ostream &operator<<(std::ostream &out, const Container &cont) {
        out << "length: " << cont.length << " width: " << cont.width
            << " height: " << cont.height << " weight: " << cont.weight
            << " value: " << cont.value;
        return out;
    }

//оператор [ ], который позволяет получить/изменить коробку по индексу.
    Box Container::operator[](int rhs) {
        Box box = vector.at(rhs);
        int length;
        int width;
        int height;
        double weight;
        int value;
        cout << "Enter length, width, height, weight, value" << endl;
        cin >> length >> width >> height >> weight >> value;
        box.setLength(length);
        box.setWidth(width);
        box.setHeight(height);
        box.setWeight(weight);
        box.setValue(value);
        return box;
    }

    bool Container::operator==(const Container &rhs) const {
        return vector == rhs.vector &&
               length == rhs.length &&
               width == rhs.width &&
               height == rhs.height &&
               weight == rhs.weight &&
               value == rhs.value;
    }

    bool Container::operator!=(const Container &rhs) const {
        return !(rhs == *this);
    }
}

