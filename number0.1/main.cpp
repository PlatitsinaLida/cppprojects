
#include <iostream>
#include "Box.h"
#include "Container.h"
using namespace std;
using namespace lp;
bool more(Box box, Box box1) {
    if(box.getHeight()> box1.getHeight() && box.getLength()> box1.getLength() &&
       box.getValue()> box1.getValue() && box.getWidth()> box1.getWidth() &&
       box.getWeight()> box1.getWeight())
    { return true;}
    else
    {
        return false;
    }
}

bool moreOrEqual(Box box, Box box1) {
    if(box.getHeight()>=box1.getHeight() && box.getLength()>=box1.getLength() &&
       box.getValue()>= box1.getValue() && box.getWidth()>=box1.getWidth() &&
       box.getWeight()>=box1.getWeight())
    { return true;}
    else
    {
        return false;
    }
}
// функция, которая проверяет, что все коробки массива можно вложить друг в друга по одной штуке

bool checkAllBox(Box box[]){
    Box flag = Box(0, 0, 0, 0, 0);
    for(int i = 0;i<sizeof(box);i++){
        for(int j = (sizeof(box)-1);j>=(i+1);j--){
            if(more(box[j],box[j-1])){
                flag = box[j];
                box[j]=box[j-1];
                box[j-1]=flag;
            }
        }
    }
    for(int i=0; i< sizeof(box); i++)
    {
        if(moreOrEqual(box[i],box[i+1])==true)
        {return false;}
    }
    return true;
}
// функция, которая для массива коробок вычисляет суммарную стоимость содержимого всех коробок.
int Sum (Box box[]) {
    int sum = 0;
    for(int i=0; i<sizeof(box); i++)
    {
        sum += box[i].getValue();
    }
    return sum;
}

// функция, которая проверяет, что сумма длины, ширины и высоты всех коробок не превосходит заданного значения.
bool SumLenWidHeig(Box box[], int number) {
    int sum = 0;
    bool flag[sizeof(box)];
    for (int i = 0; i < sizeof(box); i++) {
        sum += box[i].getLength() + box[i].getWidth() + box[i].getHeight();

        if (sum <= number)
        {
            flag[i] = true;
        }
        else
        {
            flag[i] =false;
        }
    }
    for (int i = 0; i < sizeof(flag); i++)
    {
        if (flag[i] = false)
        {
            return false;
        }
    }
    return true;
}

// функция, которая определяет максимальный вес коробок, объем которых не больше параметра maxV.
double MaxWeight(Box box[], int maxV)
{
    double totalWeight = 0.0;
    for (int i = 0; i < sizeof(box); i++)
    {
        int V = box[i].getLength() * box[i].getWidth() * box[i].getHeight();
        if (V >= maxV)
        {
            totalWeight += box[i].getWeight();
        }
    }
    return totalWeight;
}


int main() {
    int length=0, width=0, height=0,value=0;
    double weight=0.0;
    Box box= Box(length,width,height,weight,value);
    Box box1=Box(1,3,2,0.5,4);
    Box box2=Box(2,2,3,1.5,6);
    Box box3=Box(6,5,5,0.6,7);
    Box box4=Box(5,7,8,2.7,5);
    cin>>box;
    cout<<box<<endl;
    Box boxes[5] = {box, box1,box2,box3,box4};
   cout<<"Check all boxes: "<< checkAllBox(boxes)<<endl;
   cout<<"Sum: "<<Sum(boxes)<<endl;
   cout<<"Sum not more number: "<<SumLenWidHeig(boxes,100)<<endl;
   cout<<"Max Weight: "<<MaxWeight(boxes,250)<<endl;

   Container contain = Container(0,0,0,0.0,0);
    cin>>contain;
   contain.addContainer(box);
   contain.addContainer(box1);
   contain.addContainer(box2);
   contain.addContainer(box3);
   contain.addContainer(box4);
   cout<<contain;
    contain.clearElem(4);
    cout<<contain;
   cout<<"Number of boxes in a container: "<<contain.count()<<endl;
   cout<<"Total weight of container contents: "<<contain.countWeight()<<endl;
   cout<<"Total cost of content: "<<contain.countValue()<<endl;
   cout<<"Third container element: "<<contain[2]<<endl;
   cout<<"First container element: "<<contain.getBoxByIndex(0)<<endl;

return 0;

}
