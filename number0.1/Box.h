
#ifndef NUMBER0_1_BOX_H
#define NUMBER0_1_BOX_H


#include <ostream>

/*struct Box
{
private: int length;
    int width;
    int height;
    double weight;
    int value;

    Box(int leng, int wid, int heig, double weig, int val);

public:   int getLength(void);
    int getWidth(void);
    int getHeight(void);
    double getWeight(void);
    int getValue(void);
    bool moreOrEqual(Box box, Box box1);
    bool more(Box box, Box box1);

    int Sum (Box box[]);
    bool SumLenWidHeig(Box box[], int number);
    double MaxWeight(Box box[], int maxV);
    bool checkAllBox(Box box[]);

};
*/


namespace lp {
    class Box {
    private:
        int length;
        int width;
        int height;
        double weight;
        int value;


    public:
        Box(int length, int wid, int heig, double weig, int val);


        bool operator==(const Box &rhs) const;

        bool operator!=(const Box &rhs) const;

        friend std::istream &operator>>(std::istream &is, Box &box);

        friend std::ostream &operator<<(std::ostream &os, const Box &box);

        int getLength();

        void setLength(int length);

        int getWidth();

        void setWidth(int width);

        int getHeight();

        void setHeight(int height);

        double getWeight();

        void setWeight(double weight);

        int getValue();

        void setValue(int value);

    };
}
#endif //NUMBER0_BOX_H

