#include "Tree.h"

void Tree::copyTree(TreeElem *& root, TreeElem * copy)
{
    if (!copy) { return; }
    root = new TreeElem(copy->data, nullptr, nullptr);
    copyTree(root->left, copy->left);
    copyTree(root->right, copy->right);

}

void Tree::delTree(TreeElem * root)
{
    if (!root) { return; }
    delTree(root->left);
    delTree(root->right);
    delete root;
}

void Tree::writeTree(std::ostream & os, TreeElem * root, int step)
{
    if (!root) { return; }
    for (int i = 0; i < step; i++) {
        os << "\t";
    }
    os << root->data << "\n";
    writeTree(os, root->left, step + 1);
    writeTree(os, root->right, step + 1);
}

//количество четных чисел в дереве
int Tree::countEven(TreeElem * root)
{

    if (!root) { return 0; }
    if (root->data % 2 == 0) { return 1 + countEven(root->left) + countEven(root->right); }
    return countEven(root->left) + countEven(root->right);
}

//проверка того, что в дереве только положительные числа
bool Tree::positiveElem(TreeElem * root)
{
    if (!root) { return true; }
    if (root->data >= 0) { return positiveElem(root->left) && positiveElem(root->right); }
    return false;
}

//удаление в дереве всех листьев
void Tree::delLeaves(TreeElem *& root)
{
    if (!root) { return; }
    if (!root->left && !root->right) {
        delete root;
        root = nullptr;
    }
    else {
        delLeaves(root->left);
        delLeaves(root->right);
    }
}

//среднее арифметическое всех чисел в дереве
double Tree::middle(TreeElem * root)
{
    if (!root) { return 0; }
    return root->data + middle(root->left) + middle(root->right);
}

//поиск заданного элемента x в дереве
bool Tree::findElem(int x, TreeElem * root, std::vector<int> &vect)
{
    if (!root) { return false; }
    if (root->data == x) { return true; }
    vect.push_back(0);//закидываем 0 в конец, идем влево
    if (findElem(x, root->left, vect)) {
        return true;
    }
    vect.pop_back();//выкинуть последний элемент
    vect.push_back(1);//идем вправо
    if (findElem(x, root->right, vect)) {
        return true;
    }
    vect.pop_back();
    return false;
}

//конструктор
Tree::Tree()
{
    root = nullptr;
    size = 0;
}

//конструктор
Tree::Tree(const Tree & copy)
{
    copyTree(root, copy.root);
    size = copy.size;
}

//конструктор
Tree::Tree(Tree && copy)
{
    std::swap(root, copy.root);
    size = copy.size;
}

//оператор присваивания
Tree & Tree::operator=(const Tree & copy)
{
    if (&copy == this) {
        return *this;
    }
    delTree(root);
    root = nullptr;
    copyTree(root,copy.root);
    size = copy.size;
    return *this;
}

//деструктор
Tree::~Tree()
{
    delTree(root);
    root = nullptr;

}

//вставка элемента в дерево
void Tree::pasteElem(int elem, std::vector<int> vector)
{
    TreeElem* node;
    if (vector.size() == 0) {
        if (root) {
            root->data = elem;
        }
        else {
            root = new TreeElem(elem, nullptr, nullptr);
            size++;
        }
    }
    else {
        node = root;
        for (int i = 0; i < vector.size()-1; i++) {
            if (!node) { throw VectorException(); }
            if (vector[i] == 0) {
                node = node->left;
            }
            if (vector[i] == 1) {
                node = node->right;
            }
        }
        if (vector.back() == 0) {
            if (node->left) {
                node->left->data = elem;
            }
            else {
                node->left = new TreeElem(elem, nullptr, nullptr);
                size++;
            }
        }
        else {
            if (node->right) {
                node->right->data = elem;
            }
            else {
                node->right = new TreeElem(elem, nullptr, nullptr);
                size++;
            }
        }
    }

}

//количество четных чисел в дереве
int Tree::countEven()
{
    return countEven(root);
}

//проверка того, что в дереве только положительные числа
bool Tree::positiveElem()
{
    return positiveElem(root);

}

//удаление в дереве всех листьев
void Tree::delLeaves()
{
    delLeaves(root);
}

//среднее арифметическое всех чисел в дереве
double Tree::middle()
{
    return middle(root)/size;

}

//поиск заданного элемента x в дереве
std::vector<int> Tree::findElem(int x)
{
    std::vector<int> res = {};

    if (findElem(x, root, res)) {
        return res;
    }


}


TreeElem::TreeElem(int data, TreeElem * left, TreeElem * rigth)
{
    this->data = data;
    this->left = left;
    this->right = rigth;
}

//оператор вывода
//*Вывод дерева в поток по уровням (доп)
std::ostream & operator<<(std::ostream & stream, Tree & tree)
{
    tree.writeTree(stream, tree.root, 0);
    return stream;
}