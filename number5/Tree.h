#ifndef NUMBER5_TREE_H
#define NUMBER5_TREE_H

#include <vector>
#include <iostream>
struct VectorException {};

struct TreeElem {//элемент дерева
    int data;
    TreeElem* left;
    TreeElem* right;
    TreeElem(int data, TreeElem *left, TreeElem* rigth);
};
class Tree
{
private:
    TreeElem* root;
    int size;
    void copyTree(TreeElem*& root, TreeElem* copy);
    void delTree(TreeElem * root);
    void writeTree(std::ostream & os, TreeElem * root, int step);
    int countEven(TreeElem* root);
    bool positiveElem(TreeElem* root);
    void delLeaves(TreeElem*& root);
    double middle(TreeElem* root);
    bool findElem(int x, TreeElem* root, std::vector<int> &vect);
public:
    Tree();//конструктор
    Tree(const Tree &copy);//конструктор
    Tree(Tree &&copy);//конструктор
    Tree& operator= (const Tree &copy);//оператор присваивания
    ~Tree();//деструктор
    void pasteElem(int elem, std::vector<int> vector);//вставка элемента в дерево
    friend std::ostream& operator<<(std::ostream & stream, Tree &tree);//оператор вывода
    int countEven();//количество четных чисел в дереве
    bool positiveElem();//проверка того, что в дереве только положительные числа
    void delLeaves();//удаление в дереве всех листьев
    double middle();//среднее арифметическое всех чисел в дереве
    std::vector<int> findElem(int x);//поиск заданного элемента x в дереве
};


#endif //NUMBER5_TREE_H
