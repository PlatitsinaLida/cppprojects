#ifndef NUMBER6_TREEDICTIONARY_H
#define NUMBER6_TREEDICTIONARY_H

#include "iostream"
#include "string"

struct Node {//узлы дерева
    std::string word;
    unsigned int frequency;
    int bal;
    Node *right, *left;
    Node(std::string word, unsigned int frequency = 1U, int bal = 0, Node *right = nullptr, Node *left = nullptr);
};

class TreeDictionary
{
private:
    Node* root;
    int size;
    void del2(Node *& rootLeft, Node *& toDel);
    void copyTree(Node *root, Node *copy);
    void deleteTree(Node *root);
    unsigned int findWord(Node *root, std::string word);
    void addWord(Node *& root, std::string& word);
    void deleteWord(Node *& root, std::string& word);
    void printTree(std::ostream& os, Node* root);
public:
    TreeDictionary();//конструктор
    TreeDictionary(const TreeDictionary &copy);//конструктор
    TreeDictionary(TreeDictionary &&move);//конструктор
    ~TreeDictionary();//деструктор
    TreeDictionary& operator=(const TreeDictionary &copy);//оператор =
    TreeDictionary& operator=(TreeDictionary &&move);//оператор =
    unsigned int findWord(std::string word);//поиск слова в словаре
    void addWord(std::string word);//добавить слово в словарь
    void deleteWord(std::string word);//удалить слово из словаря
    unsigned int countWords();// общее количество слов в словаре с учетом числа вхождений слов
    friend std::ostream& operator<< (std::ostream& os, TreeDictionary& obj);//оператор вывода
};


#endif //NUMBER6_TREEDICTIONARY_H
