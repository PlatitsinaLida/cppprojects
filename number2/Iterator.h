#ifndef NUMBER2_ITERATOR_H
#define NUMBER2_ITERATOR_H

#include "RingBuffer.h"

class Iterator {
private:
    int index;
    RingBuffer *queue;
public:
    friend class RingBuffer;
    Iterator(RingBuffer* &que);//конструктор, который привязывает итератор к очереди
    void start();//начать перебор элементов
    void next();//перейти к следующему элементу
    bool finish();//проверка, все ли проитерировано
    int getValue();//получить очередной элемент очереди
};


#endif //NUMBER2_ITERATOR_H
