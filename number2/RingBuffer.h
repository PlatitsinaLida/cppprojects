#ifndef NUMBER2_RINGBUFFER_H
#define NUMBER2_RINGBUFFER_H
struct SizeException {};
struct EmptyException {};
class RingBuffer {
private:
    double *arr;
    int size;
    int firstPointer;
    int lastPointer;
    bool flag;
public:
    friend class Iterator;
    RingBuffer(int size);//консруктор
    void addElement(double element);//добавить элемент в конец очереди (при переполнении – исключение)
    double getElem();//взять элемент в начале очереди (при отсутствии – исключение)
    double seeElem();//получить элемент в начале очереди (без его изъятия)
    int getSize();//размер очереди
    void doEmptyQueue();//сделать очередь пустой
    bool checkEmpty();//проверка очереди на пустоту
};

#endif //NUMBER2_RINGBUFFER_H
