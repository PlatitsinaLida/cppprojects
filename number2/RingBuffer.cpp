#include "RingBuffer.h"
//консруктор
RingBuffer::RingBuffer(int size) {
    flag = true;
    arr = new double[size];
    this->size = size;
    firstPointer = 0;
    lastPointer = 0;

}

//добавить элемент в конец очереди (при переполнении – исключение)
void RingBuffer::addElement(double element) {
    if ((lastPointer + 1) % size == firstPointer) {
        throw SizeException();
    }
    if (flag) {
        arr[lastPointer] = element;
        flag = false;

    }
    else {
        lastPointer++;
        lastPointer = lastPointer%size;
        arr[lastPointer] = element;
    }
}

//взять элемент в начале очереди (при отсутствии – исключение)
double RingBuffer::getElem() {
    if (flag) {
        throw EmptyException();
    }
    if (firstPointer == lastPointer) {
        flag = true;
        return arr[firstPointer];
    }
    if (firstPointer != lastPointer) {
        firstPointer++;
        return arr[firstPointer - 1];
    }
}

//получить элемент в начале очереди (без его изъятия)
double RingBuffer::seeElem() {
    if (flag) {
        throw EmptyException();
    }
    return arr[firstPointer];
}

//размер очереди
int RingBuffer::getSize() {
    return size;
}

//сделать очередь пустой
void RingBuffer::doEmptyQueue() {
    firstPointer = lastPointer;
    flag = true;

}

//проверка очереди на пустоту
bool RingBuffer::checkEmpty() {
    if (flag) { return true; }
    return false;
}